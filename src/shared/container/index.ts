import "reflect-metadata"
import { container } from 'tsyringe';

import '../providers';

import IPlanetPostsRepository from '@modules/planet/repositories/IPostRepository';
import PlanetPostsRepository from '@modules/planet/infra/typeorm/repositories/PostsRepository';
import IPlanetHistoryRepository from '@modules/planet/repositories/IHistoryRepository';
import PlanetHistoryRepository from '@modules/planet/infra/typeorm/repositories/HistoryRepository';
//
import IPlanetSourcePost from '@modules/planetSource/repositories/IPostRepository';
import PlanetSuzy from '@modules/planetSource/infra/crosscutting/repositories/PlanetSuzy';

container.registerSingleton<IPlanetPostsRepository>(
  'GalleryRepository',
  PlanetPostsRepository,
);

container.registerSingleton<IPlanetHistoryRepository>(
  'GalleryHistoryRepository',
  PlanetHistoryRepository,
);

container.registerSingleton<IPlanetSourcePost>('GallerySource', PlanetSuzy);
