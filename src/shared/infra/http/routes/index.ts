import { Router } from 'express';

import planetRouter from '@modules/planet/infra/http/routes/posts.routes';
import planetSourceRouter from '@modules/planetSource/infra/http/routes/posts.routes';

const routes = Router();

routes.use('/planet', planetRouter);
routes.use('/planet-source', planetSourceRouter);

export default routes;
